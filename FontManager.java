public class FontManager {

    private static FontManager fontManagerInstance = null;

    private HashMap<String, Typeface> fontsMap = new HashMap<>();

    private FontManager() {
    }

    public static FontManager getInstance() {
        if (fontManagerInstance == null) {
            fontManagerInstance = new FontManager();
        }
        return fontManagerInstance;
    }

    public Typeface getFont(Context context, String fontName) {
        Typeface typeface = fontsMap.get(fontName);

        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
            fontsMap.put(fontName, typeface);
        }
        return typeface;
    }
}